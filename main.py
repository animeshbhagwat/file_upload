from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import os

OTHERS_FOLDER = r'upload_path/other_files'
PDF_FOLDER = r'upload_path/pdf_files'
TXT_FOLDER = r'upload_path/txt_files'

app = Flask(__name__)
app.config["other_files"] = OTHERS_FOLDER
app.config["pdf_files"] = PDF_FOLDER
app.config["txt_files"] = TXT_FOLDER


@app.route('/')
def uplaod_file():
    return render_template('upload.html')


@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        extension = f.filename.split('.')[-1]
        if extension.lower() == 'pdf':
            f.save(os.path.join(app.config["pdf_files"], secure_filename(f.filename)))
        elif extension.lower() == 'txt':
            f.save(os.path.join(app.config["txt_files"], secure_filename(f.filename)))
        else:
            f.save(os.path.join(app.config["other_files"], secure_filename(f.filename)))
        return render_template('success.html')
    else:
        return 'OK'

if __name__ == '__main__':
    app.run(debug=True)
